# XML Validation with Java

This repo aims at testing XML validation with java.

> **Requirements**: schema and XML files for test are not in this repo, but has to be loaded as maven dependency to "eu.els.sie.test:xml-multi-models".
> xml-multi-models has not been published to nexus ou Maven Central, you have to build it locally:
> 
> - git clone this repo : https://bitbucket.org/mricaud/xml-multi-models-sample
> - mvn clean install

### TODO

* Implement DTD validation (with Xerces?)
* Try a Relax NG schema containing schematron rules
* Try XSD 1.1 with assertions ?
* Test Flash EditorialEntity NVDL validation 
  * maybe here : https://bitbucket.org/mricaud/cep-xml-validation
* Continue with JAXP Wrapper ? Add test in the repo ?

### Liens 

* Jing Home Page : https://relaxng.org/jclark/jing.html
* Jing Github : https://github.com/relaxng/jing-trang/
* Java doc Jing : https://relaxng.org/jclark/api/jing/index.html
* Jim : https://github.com/jimetevenard/Jing-JAXP-Wrapper
* https://www.javadoc.io/doc/com.componentcorp.xml.validation/relaxng/latest/index.html
* Validation Schematron avec `AutoSchemaReader` : https://stackoverflow.com/questions/21705713/jing-relaxng-length-validation-for-string
* Validation RelaxNG avec XMLSchemaFactory : https://www.youtube.com/watch?v=bXwrVyaG1Ls 
* DTD ouvrage checker (Hamdi) : https://bitbucket.org/elsgestion/sie-dtd-ouvrage-checker/src/master/src/main/java/eu/els/sie/dz/dtdouvrage/checker/Run.java
  * Utilisation de Jing avec `ValidationDriver`
* `ValidationDriver` : https://stackoverflow.com/questions/10835208/jing-relaxng-validator-and-custom-datatype-library-from-java-code
* Validation RNG/RNC avec [XML/Compact]SchemaFactory (JAXP) : https://stackoverflow.com/questions/4983057/using-jing-with-google-app-engine-cant-load-schemafactory-given-relax-ng-schem
* https://stackoverflow.com/questions/1541253/how-to-validate-an-xml-document-using-a-relax-ng-schema-and-jaxp
* Exemples de code (ne pas s'y fier forcément, ça a l'air d'un test) : https://www.javatips.net/api/wicket-stuff-markup-validator-master/jing/src/main/java/com/thaiopensource/validation/Validator2.java : 
* Validation tool : https://github.com/aerhard/xml-tools/blob/master/xml-tools-server/src/main/java/com/aerhard/xml/tools/ValidationThread.java
* Calabash validation NVDL : https://github.com/ndw/xmlcalabash1/blob/saxon96/src/main/java/com/xmlcalabash/extensions/NVDL.java
* Jing-Trang Github Memory, ça parle de resolver Saxon/Jing : https://githubmemory.com/repo/relaxng/jing-trang/issues?cursor=Y3Vyc29yOnYyOpK5MjAxOC0wMi0xNFQwNjo1MDo1MSswODowMM4Rso-R&pagination=next&page=3
* Protocol classpath : https://stackoverflow.com/questions/861500/url-to-load-resources-from-the-classpath-in-java
* https://stackoverflow.com/questions/861500/url-to-load-resources-from-the-classpath-in-java
* https://javarevisited.blogspot.com/2014/07/how-to-load-resources-from-classpath-in-java-example.html#axzz6xIShLEpm
* https://blog.frankel.ch/xml-validation-with-importedincluded-schemas/
* https://stackoverflow.com/questions/2342808/how-to-validate-an-xml-file-using-java-with-an-xsd-having-an-include
* https://stackoverflow.com/questions/1732161/parsing-schema-in-java-with-imports-and-includes
* https://xmlresolver.org/
  * https://github.com/xmlresolver/xmlresolver#lies-damned-lies-and-uris 
  * https://github.com/xmlresolver/xmlresolver/blob/main/src/main/java/org/xmlresolver/Resolver.java
* https://stackoverflow.com/questions/15203439/how-to-convert-inputstream-into-source
* Des article très interessant sur java (gestion des exceptions etc.) : https://stackify.com/content/java/page
  * https://stackify.com/best-practices-exceptions-java
  * https://stackify.com/types-of-exceptions-java
  * https://stackify.com/java-custom-exceptions/
  * https://stackify.com/three-favorite-open-source-java-libraries
  * https://stackify.com/java-logs-types
  * https://stackify.com/whats-new-in-java-10
  * https://stackify.com/java-vs-python
  * https://stackify.com/service-locator-pattern
  * https://stackify.com/optional-parameters-java
  * https://stackify.com/java-stack-trace
  * https://stackify.com/java-blogs-for-programmers-of-all-levels
  * https://stackify.com/java-xml-jackson
  * https://stackify.com/memory-leaks-java
  * https://stackify.com/what-is-spring-boot
* https://github.com/aerhard/xml-tools/blob/master/xml-tools-server/src/main/java/com/aerhard/xml/tools/ValidationThread.java
* Spring Core tuto Article : https://github.com/eugenp/tutorials/tree/master/spring-core