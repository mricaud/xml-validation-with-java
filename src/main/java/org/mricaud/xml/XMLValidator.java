package org.mricaud.xml;

import com.thaiopensource.resolver.Resolver;
import com.thaiopensource.resolver.catalog.CatalogResolver;
import com.thaiopensource.util.PropertyMap;
import com.thaiopensource.util.PropertyMapBuilder;
import com.thaiopensource.validate.ValidateProperty;
import com.thaiopensource.validate.ValidationDriver;
import com.thaiopensource.xml.sax.ErrorHandlerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class XMLValidator {

    private static final ClassLoader CL = XMLValidator.class.getClassLoader();
    private static final Logger logger = LoggerFactory.getLogger(XMLValidator.class);
    //public static MyErrorHandler errorHandler = new MyErrorHandler();
    public static ErrorHandler errorHandler = new ErrorHandlerImpl();

    // Declare handler for cp protocol
    // cp protocol is needed to resolve schema import within a schema included as maven dependency
    static {
        System.setProperty("java.protocol.handler.pkgs", "org.mricaud.xml");
    }

    public static ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public static void validateRNG(String xmlURI, String schemaURI, List<String> xmlCatalogsPath) throws Exception {
        validateXML(xmlURI, schemaURI, xmlCatalogsPath);
    }

    public static void validateRNC(String xmlURI, String schemaURI, List<String> xmlCatalogsPath) throws Exception {
        validateXML(xmlURI, schemaURI, xmlCatalogsPath);
    }

    public static void validateSchematron(String xmlURI, String schemaURI, List<String> xmlCatalogsPath) throws Exception {
        validateXML(xmlURI, schemaURI, xmlCatalogsPath);
    }

    public static void validateXSD(String xmlURI, String schemaURI, List<String> xmlCatalogsPath) throws Exception {
        validateXML(xmlURI, schemaURI, xmlCatalogsPath);
    }

    public static void validateNVDL(String xmlURI, String schemaURI, List<String> xmlCatalogsPath) throws Exception {
        validateXML(xmlURI, schemaURI, xmlCatalogsPath);
    }

    /*
     * Validate any XML file with RNG, RNC, Schematron1.5, Iso-Schematron, NVDL, but not with DTD
     * @param xmlURI is a string, it might use cp protocol : cp:/path/to/xml.xml
     * @param schemaURI is a string, it might use cp protocol : cp:/path/to/xml.xml
     * @param xmlCatalogsPath is a list of strings without protocol, to be loaded with classLoader (no cp protocol)
     * Any relativ URI or cp:/ URI will work within xmlURI/schemaURI but not within xmlCatalogsPath
    */
    private static void validateXML(String xmlURI, String schemaURI, List<String> xmlCatalogsPath)
            throws Exception {
        // XML file
        InputSource xmlAsInputSource = new InputSource(xmlURI);
        // Schema
        InputSource schemaAsInputSource = new InputSource(schemaURI);
        // PropertyMap
        PropertyMap propertyMap = createPropertyMap(xmlCatalogsPath, errorHandler);
        // Set validationDriver with the propertyMap
        ValidationDriver validationDriver = new ValidationDriver(propertyMap);
        // load schema
        try { validationDriver.loadSchema(schemaAsInputSource); }
        catch (IOException e) {
            throw new Exception("Unable to load Schema from schemaURI", e);
        }
        catch (SAXException e) {
            throw new Exception("Schema loaded with errors", e);
        }
        // validate
        try { validationDriver.validate(xmlAsInputSource); }
        catch (IOException e) {
            throw new Exception("Unable to load XML from xmlURI", e);
        }
        catch (SAXException e) {
            throw new Exception("XML loaded with errors", e);
        }

    }


    private static PropertyMap createPropertyMap(List<String> xmlCatalogsPath, ErrorHandler eh) throws Exception {

        PropertyMapBuilder propertyMapBuilder = new PropertyMapBuilder();

        // Set PropertyMap RESOLVER with catalogs
        if (xmlCatalogsPath != null) {
            List<String> catalogs = new ArrayList<>();
            for (String s : xmlCatalogsPath) {
                try {
                    catalogs.add(CL.getResource(s).toURI().toASCIIString());
                } catch (URISyntaxException e) {
                    //logger.error("Unable to load XML catalog : ", e);
                    throw new Exception("Catalog URI is not recognized", e);
                }
            }
            Resolver catalogResolver = new CatalogResolver(catalogs);
            propertyMapBuilder.put(ValidateProperty.RESOLVER, catalogResolver);
        }

        // SET PropertyMap ERROR_HANDLER
        propertyMapBuilder.put(ValidateProperty.ERROR_HANDLER, eh);

        // Return
        return propertyMapBuilder.toPropertyMap();
    }


}
