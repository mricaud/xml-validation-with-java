package org.mricaud.xml.cp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class Handler extends URLStreamHandler {
    private static final ClassLoader CL = Handler.class.getClassLoader();

    @Override
    protected URLConnection openConnection(URL url) throws IOException {
        return new URLConnection(url) {
            @Override
            public void connect() throws IOException {
                throw new IOException("Should not be here...");
            }

            @Override
            public InputStream getInputStream() throws IOException {
                return CL.getResourceAsStream(url.getPath().substring(1));
            }
        };
    }
}
