package org.mricaud.xml;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyErrorHandler implements ErrorHandler {

    static enum LEVEL { WARN, ERROR, FATAL }

    private Map<LEVEL, List<SAXParseException>> report = new HashMap<>();

    public Map<LEVEL, List<SAXParseException>> getReport() {
        return report;
    }

    public MyErrorHandler() {
        report.put(LEVEL.WARN, new ArrayList<>());
        report.put(LEVEL.ERROR, new ArrayList<>());
        report.put(LEVEL.FATAL, new ArrayList<>());
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        report.get(LEVEL.WARN).add(exception);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        report.get(LEVEL.ERROR).add(exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        report.get(LEVEL.FATAL).add(exception);
    }
}
