package org.mricaud.xml;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.jupiter.api.DisplayName;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.fail;

public class XMLValidatorTest {

    private  static List<String> xmlCatalogsPathLoadedFromDependency = Arrays.asList(
                "xml-multi-models-sample/main/catalogs/catalog-for-dependency.xml",
                "xml-multi-models-sample/main/catalogs/catalog-for-dtd.xml"
    );

    private  static List<String> xmlCatalogsPathLocal = Arrays.asList(
            "catalog-local.xml"
    );

    private static final Logger logger = LoggerFactory.getLogger(XMLValidatorTest.class);

    /* ==================================*/
    /* RELAX NG Compact */
    /* ==================================*/

    @Test
    @DisplayName("Test Relax NG Validation : valid XML - simple RNC - no catalog")
    void validationRNC_xmlValid_rncSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNC("cp:/xml-multi-models-sample/test/rnc/simple-book-valid-rnc.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rnc/simple-book.rnc",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    /* ==================================*/
    /* RELAX NG XML */
    /* ==================================*/

    @Test
    @DisplayName("Test Relax NG Validation : valid XML - simple RNG - no catalog")
    void validationRNG_xmlValid_rngSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/simple-book-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/simple-book/simple-book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : invalid XML - simple RNG - no catalog")
    void validationRNG_xmlUnvalid_rngSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/simple-book-invalid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/simple-book/simple-book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML - RNG with include - no catalog")
    void validationRNG_xmlvalid_rng_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML with DOCTYPE - RNG with include - no catalog")
    void validationRNG_xmlValid_DOCTYPE_rng_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-with-doctype-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML with DOCTYPE - RNG with include - no catalog")
    void validationRNG_xmlValid_DOCTYPE_entities_rng_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-with-doctype-with-entities-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML with DOCTYPE  with unparsed entity - RNG with include - no catalog")
    void validationRNG_xmlValid_DOCTYPE_unparseEntities_rng_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-with-doctype-with-unparsed-entities-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML with DOCTYPE needs catalog with unparsed entity - RNG with include - xmlCatalogsPathLocal")
    void validationRNG_xmlValid_DOCTYPE_needsCatalog_unparseEntities_rng_xmlCatalogsPathLocal() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-with-doctype-needs-catalog-with-entities-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    xmlCatalogsPathLocal);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Relax NG Validation : valid XML with DOCTYPE needs catalog with unparsed entity - RNG with include - xmlCatalogsPathLoadedFromDependency")
    void validationRNG_xmlValid_DOCTYPE_needsCatalog_unparseEntities_rng_xmlCatalogsPathLoadedFromDependency() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateRNG("cp:/xml-multi-models-sample/test/rng/book-with-doctype-needs-catalog-with-entities-valid-rng.xml",
                    "cp:/xml-multi-models-sample/main/grammars/rng/book/book.rng",
                    xmlCatalogsPathLoadedFromDependency);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    /* ==================================*/
    /* XSD */
    /* ==================================*/

    @Test
    @DisplayName("Test XSD Validation : valid XML - Simple XSD - no Catalog")
    void validationXSD_validXML_simpleXSD_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateXSD("cp:/xml-multi-models-sample/test/xsd/simple-book-valid-xsd.xml",
                    "cp:/xml-multi-models-sample/main/grammars/xsd/simple-book.xsd",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test XSD Validation : invalid XML - Simple XSD - no Catalog")
    void validationXSD_invalidXML_simpleXSD_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateXSD("cp:/xml-multi-models-sample/test/xsd/simple-book-invalid-xsd.xml",
                    "cp:/xml-multi-models-sample/main/grammars/xsd/simple-book.xsd",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test XSD Validation : valid XML - XSD with inclusion - no Catalog")
    void validationXSD_validXML_XSDwithInclusion_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateXSD("cp:/xml-multi-models-sample/test/xsd/book-valid-xsd.xml",
                    "cp:/xml-multi-models-sample/main/grammars/xsd/book/book.xsd",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test XSD Validation : invalid XML - XSD with inclusion - no Catalog")
    void validationXSD_invalidXML_XSDwithInclusion_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateXSD("cp:/xml-multi-models-sample/test/xsd/book-invalid-xsd.xml",
                    "cp:/xml-multi-models-sample/main/grammars/xsd/book/book.xsd",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    /* ==================================*/
    /* Schematron */
    /* ==================================*/

    @Test
    @DisplayName("Test Schematron Validation : Invalid XML - simple schematron 1.5 no inclusion - no catalog")
    void validationRNG_xmlValid_sch15Simple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateSchematron("cp:/xml-multi-models-sample/test/sch/book-invalid-sch.xml",
                    "cp:/xml-multi-models-sample/main/grammars/sch/book-sch1.5.sch",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Schematron Validation : Invalid XML - simple schematron no inclusion - no catalog")
    void validationRNG_xmlInvalid_schSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateSchematron("cp:/xml-multi-models-sample/test/sch/book-invalid-sch.xml",
                    "cp:/xml-multi-models-sample/main/grammars/sch/book-no-xslt.sch",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Schematron Validation : invalid XML - schematron with xsl:include 1 level - no catalog")
    void validationRNG_xmlInvalid_schWithXslInclude1Level_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateSchematron("cp:/xml-multi-models-sample/test/sch/book-valid-sch.xml",
                    "cp:/xml-multi-models-sample/main/grammars/sch/book-xslt-include-1-level.sch",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Schematron Validation : valid XML - schematron with xsl:include 2 levels - no catalog")
    void validationRNG_xmlValid_schWithXslInclude2Levels_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateSchematron("cp:/xml-multi-models-sample/test/sch/book-valid-sch.xml",
                    "cp:/xml-multi-models-sample/main/grammars/sch/book-xslt-include-2-levels.sch",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test Schematron Validation : valid XML - schematron matching text node - no catalog")
    void validationRNG_xmlValid_schMatchingTextNode_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateSchematron("cp:/xml-multi-models-sample/test/sch/book-valid-sch.xml",
                    "cp:/xml-multi-models-sample/main/grammars/sch/match-text.sch",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    /* ==================================*/
    /* NVDL */
    /* ==================================*/

    @Test
    @DisplayName("Test NVDL Validation : valid XML - simple NVDL (rng + sch) - no catalog")
    void validationNVDL_xmlValid_nvdlSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateNVDL("cp:/xml-multi-models-sample/test/nvdl/book-valid-nvdl.xml",
                    "cp:/xml-multi-models-sample/main/grammars/nvdl/book-and-schematron.nvdl",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test NVDL Validation : invalid XML - simple NVDL (rng + sch) - no catalog")
    void validationNVDL_xmlInvalid_nvdlSimple_noCatalog() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateNVDL("cp:/xml-multi-models-sample/test/nvdl/book-invalid-nvdl.xml",
                    "cp:/xml-multi-models-sample/main/grammars/nvdl/book-and-schematron.nvdl",
                    null);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test NVDL Validation : valid XML - NVDL with dependencies (rng + rnc calling dependency:/ + sch) - xmlCatalogsPathLocal")
    void validationNVDL_xmlValid_nvdlWithDependencies_xmlCatalogsPathLocal() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateNVDL("cp:/xml-multi-models-sample/test/nvdl/book-augmented-valid-nvdl.xml",
                    "cp:/xml-multi-models-sample/main/grammars/nvdl/book-augmented.nvdl",
                    xmlCatalogsPathLocal);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }

    @Test
    @DisplayName("Test NVDL Validation : valid XML - NVDL with dependencies (rng + rnc calling dependency:/ + sch) - xmlCatalogsPathLoadedFromDependency")
    void validationNVDL_xmlValid_nvdlWithDependencies_xmlCatalogsPathLoadedFromDependency() {
        XMLValidator xmlValidator = new XMLValidator();
        try {
            xmlValidator.validateNVDL("cp:/xml-multi-models-sample/test/nvdl/book-augmented-valid-nvdl.xml",
                    "cp:/xml-multi-models-sample/main/grammars/nvdl/book-augmented.nvdl",
                    xmlCatalogsPathLoadedFromDependency);
        } catch (Exception e) {
            logger.error("ERROR: ", e);
            fail("No Exception expected:" + e.getMessage());
        }
    }



}